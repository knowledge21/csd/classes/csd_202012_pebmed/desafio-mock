package br.com.k21;

import java.util.List;

import br.com.k21.dao.VendaRepository;
import br.com.k21.modelo.Venda;

public class CalculadoraRoyalties {
	
	VendaRepository repositoryQueEssaClasseVaiUsar;
	
	public CalculadoraRoyalties(VendaRepository repositoryRecebidaPorParametro) {
		repositoryQueEssaClasseVaiUsar = repositoryRecebidaPorParametro;
	}
	
	public CalculadoraRoyalties() {
		// TODO Auto-generated constructor stub
	}

	public double calcularRoyalties(int mes, int ano) {
		
		List<Venda> vendas = BuscarVendas(mes, ano);
		double faturamento = CalcularOFaturamento(vendas);
		double comissoes = SomarComissoes(vendas);
		
		return 0.2 * (faturamento - comissoes);
	}

	private double SomarComissoes(List<Venda> vendas) {
		double comissao = 0;
		
		for (Venda venda : vendas) {
			comissao = comissao + CalculadoraComissao.Calcular(venda.getValor());
		}
		
		return comissao;
	}

	private double CalcularOFaturamento(List<Venda> vendas) {

		double faturamento = 0;
		
		for (Venda venda : vendas) {
			faturamento = faturamento + venda.getValor();
		}
		
		return faturamento;
	}

	private List<Venda> BuscarVendas(int mes, int ano) {
		List<Venda> vendas = repositoryQueEssaClasseVaiUsar.obterVendasPorMesEAno(ano, mes);
		return vendas;
	}
}
